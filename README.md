# Linux From Scratch Русский

Перевод "Linux From Scratch" на русский язык.

- releases-com - перевод конкретных выпусков в техническом стиле, с использованием словарей, с которыми соглашаются IT-сообщества, а так же с использованием более старых переводов.

Перевод версии
- 9.0 https://github.com/avmaisak/LFS_Book
- 10.0 https://github.com/zlietapki/linux_from_scratch_10_rus


- https://gitlab.com/AL_Tron_/Translations/-/tree/master/linuxfromscratch.org/lfs
  - Устаревшие директории, которые больше не используются:
    - head-com - dev-версия в стиле technical-community(com)
    - head - dev-версия в дословном, техническом и литературном стиле
    - releases/10.1 - выпуск версии 10.1 в дословном, техническом и литературном стиле
        - com - в стиле technical-community(com)
  - Текущие директории
    - releases-min - перевод выпусков в смешанном стиле min-etymology и literal


Максимальная длина строки в xml файле - 184?
Иначе текст может пропасть при генерации в некоторых местах.

Чтобы понять, были ли изменения в оригинальном тексте, например, между 10.1 и 11 версией, можно воспользоваться инструментом diff:

https://wiki.linuxfromscratch.org/lfs/changeset?old=9fe802c&old_path=%2F&new=15a3b47e9d5a57187dacea296d3f26e730f0746c&new_path=%2F

Таким образом можно узнать, что актуально в старом переводе, а что нет.

Поддержать:
- Яндекс кошелёк 410011604674676 (указать в переводе "Н(З)а перевод на русский Linux From Scratch")

Вопросы и предложения, можно отправлять на почту:
altron.dev@gmail.com

Обсуждение перевода:
https://t.me/LinuxFromScratch_RUS
